//WAP to find the distance between two point using 4 functions.
#include <stdio.h>

#include <math.h>

float process(float x1, float y1, float x2, float y2) {
        float d = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
      

        return d;
}

float input(char c, int n) {
        printf("enter %c%d : ", c, n);
        float a;
        scanf("%f", & a);

        return a;
}

void output(float a) {
        printf("distance= %f", a);
}

int main() {
        float x1 = input('x', 1);
        float y1 = input('y', 1);
        float x2 = input('x', 2);
        float y2 = input('y', 2);

        float res = process(x1, y1, x2, y2);
        output(res);

        return 0;
}