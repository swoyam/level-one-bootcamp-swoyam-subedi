//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>

#include <math.h>

float process(int h, int b, int d) {
        float v = ((h * d) + d) / (3 * b);

        return v;
}

float input(char c) {
        printf("enter %c: ", c);
        float a;
        scanf("%f", & a);

        return a;
}

void output(float a) {
        printf("volume of trumbolide= %f", a);
}

int main() {
        float h = input('h');
        float b = input('b');
        float d = input('d');

        float res = process(h, b, d);
        output(res);

        return 0;
}